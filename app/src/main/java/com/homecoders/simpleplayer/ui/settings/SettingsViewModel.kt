package com.homecoders.simpleplayer.ui.settings

import androidx.lifecycle.ViewModel
import javax.inject.Inject

class SettingsViewModel @Inject constructor() : ViewModel()