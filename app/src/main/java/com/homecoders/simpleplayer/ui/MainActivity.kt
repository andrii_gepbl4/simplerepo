package com.homecoders.simpleplayer.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.FragmentTransaction
import com.homecoders.simpleplayer.R
import com.homecoders.simpleplayer.ui.player.PlayerFragment
import com.homecoders.simpleplayer.ui.playlist.PlaylistFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.flFragmentContainer, PlayerFragment.newInstance())
        transaction.commit()
    }
}
