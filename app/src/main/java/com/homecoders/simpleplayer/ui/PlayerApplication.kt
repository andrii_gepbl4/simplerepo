package com.homecoders.simpleplayer.ui

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class PlayerApplication : Application()