package com.homecoders.simpleplayer.ui.filestorage

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.homecoders.simpleplayer.R
import com.homecoders.simpleplayer.databinding.FragmentFileStorageBinding
import com.homecoders.simpleplayer.ui.player.PlayerViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FileStorageFragment : Fragment() {
    private lateinit var binding: FragmentFileStorageBinding
    private val viewModel: FileStorageViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFileStorageBinding.inflate(inflater, container, false)
        return binding.root
    }
}