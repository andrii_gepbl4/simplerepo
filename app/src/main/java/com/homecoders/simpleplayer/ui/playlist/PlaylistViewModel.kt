package com.homecoders.simpleplayer.ui.playlist

import androidx.lifecycle.ViewModel
import javax.inject.Inject

class PlaylistViewModel @Inject constructor() : ViewModel()